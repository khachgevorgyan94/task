import React, {Component} from 'react';

import './style.scss';
import './responsive.scss';
import {NavLink} from "react-router-dom";
import Settings from "../../platform/serivces/settings";
import RouteServices from "../../platform/serivces/route-services";


class Sidebar extends Component {

  state = {
    isOpenMenu: false,
    defaultLanguage: {
      name: '',
      value: ''
    },
    sideBarList: null
  }

  componentDidMount() {
    window.addEventListener('openMenu', this.openMenu);
    this.setState({
      sideBarList: RouteServices.getSideBarList()

    })

  }

  changeLanguage = async (language) => {
    Settings.language = language
  }

  openMenu = () => {
    this.setState({isOpenMenu: true})
  }
  closeMenu = () => {
    this.setState({isOpenMenu: false})

  }

  render() {
    const {sideBarList} = this.state

    return (
        <>
          <div
              className={`sidenav-block G-flex G-flex-column G-align-center G-text-right G-mb-6`}>
            <div className='P-sidebar-header'/>
            <div className="P-sidenav-list G-flex G-flex-column G-align-start G-justify-between">
              <div className="P-menu-list">
                {sideBarList && sideBarList.length ? sideBarList.map((item, index) => {
                  return <NavLink key={index} onClick={this.closeMenu} activeClassName="activeLink" title={item.name}
                                  to={item.path} className="P-menu-item G-flex G-align-center">
                    {item.icon ? <i className='P-menu-icon' style={{backgroundImage: `url('${item.icon}')`}}/> : null}
                    <p>{item.name}</p></NavLink>
                }) : null}
              </div>
            </div>
          </div>
        </>
    );
  }
}

export default Sidebar;


