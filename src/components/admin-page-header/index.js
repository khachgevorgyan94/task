import React, {Component} from 'react';
import './style.scss';
import './responsive.scss';


class HeaderComponent extends Component {

  render() {
    const {headerTitle} = this.props;
    return (
        <div className="G-admin-header G-flex G-align-center G-justify-between">

          <div className="G-admin-title">
            <h3>{headerTitle}</h3>
          </div>
        </div>

    );
  }
};
export default HeaderComponent;
