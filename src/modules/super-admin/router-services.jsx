import ROUTES from "../../platform/constants/routes";
import SuperAdminUsersMain from "./users/pages/main";
import usersIcon from '../../assets/images/svg/users.svg'
import SuperAdminUsersDetails from "./users/pages/details";

export const routerListSuperAdmin = {
  role: 'SuperAdmin',
  pages: [
    {
      path: ROUTES.SUPER_ADMIN.USERS.MAIN,
      component: SuperAdminUsersMain,
      exact: true,
    },
    {
      path: ROUTES.SUPER_ADMIN.USERS.DETAILS,
      component: SuperAdminUsersDetails,
      exact: true,
    },
    {
      path: ROUTES.SUPER_ADMIN.USERS.CREATE,
      component: SuperAdminUsersDetails,
      exact: true,
    },

  ]
}

export const SuperAdminSideBar = [
  {
    id: 1,
    name: 'Users',
    path: ROUTES.SUPER_ADMIN.USERS.MAIN,
    icon: usersIcon,
  },
  {
    id: 1,
    name: 'test 1',
    icon: usersIcon,
    path: ROUTES.SUPER_ADMIN.USERS,

  },
  {
    id: 1,
    name: 'test 2',
    icon: usersIcon,
    path: ROUTES.SUPER_ADMIN.USERS,

  },
  {
    id: 1,
    name: 'test 3',
    icon: usersIcon,
    path: ROUTES.SUPER_ADMIN.USERS,

  },
  {
    id: 1,
    name: 'test 4',
    icon: usersIcon,
    path: ROUTES.SUPER_ADMIN.USERS,

  },
  {
    id: 1,
    name: 'test 5',
    icon: usersIcon,
    path: ROUTES.SUPER_ADMIN.USERS,

  },


]
