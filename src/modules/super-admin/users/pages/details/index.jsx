import React, {Component} from 'react';
import HeaderComponent from "../../../../../components/admin-page-header";
import './style.scss';
import PageLoader from "../../../../../components/page-loader";

import uploadIcon from '../../../../../assets/images/upload-image.png'
import ButtonLoader from "../../../../../components/loader-button";
import UserValidation from "../../../../../platform/serivces/validations/user-validation";
import UsersController from "../../../../../platform/api/users";

class SuperAdminUsersDetails extends Component {
  state = {
    pageLoading: false,
    data: null,
    photo: null,
    form: {
      name: '',
      location: '',
      email: '',
    },

    errorList: {
      name: false,
      email: false,
      location: false,
      incorrectEmail: false,
    },
    isValidationForm: null,
    isLoading: false,
    userId: null,
    photoData: null
  }

  componentDidMount() {
    const {id} = this.props.match.params
    this.setState({userId: id})

    if (id) {
      this.getUserDetails(id)
    }
  }


  coverUpload = (e) => {
    const {files} = e.currentTarget;
    const {form} = this.state;
    if (files && files.length) {
      this.setState({
        photo: URL.createObjectURL(files[0]),
        photoData: files[0]
      })
      this.setState({form});

    }
  };

  submit = () => {
    this.setState({isLoading: true, isValidationForm: UserValidation.isValidation(this.state.form)}, () => {
      if (this.state.isValidationForm.isValidation) {
        if (this.state.userId) {
          this.editUser()
        } else {
          this.createUser()
        }
      } else {
        this.setState({
          errorList: this.state.isValidationForm.errorList, isLoading: false
        })
      }
    })
  }
  createUser = async () => {
    const {form} = this.state

    this.setState({isLoading: true}, async () => {
      const promiseList = []
      promiseList.push(UsersController.createUser(form))
      const form_data = new FormData()
      form_data.append('photo', this.state.photoData)
      promiseList.push(UsersController.uploadImage(form_data))
      await Promise.all(promiseList)
      this.setState({isLoading: false})
      this.props.history.goBack()
    })
  }

  editUser = async () => {
    const {form} = this.state

    this.setState({isLoading: true}, async () => {
      const promiseList = []
      promiseList.push(UsersController.editUser(form))
      const form_data = new FormData()
      form_data.append('file', this.state.photoData)
      promiseList.push(UsersController.uploadImage(form.id, form_data))

      await Promise.all(promiseList)
      this.setState({isLoading: false})
      this.props.history.goBack()
    })
  }


  changeInput = (e) => {
    const {form} = this.state;
    form[e.currentTarget.name] = e.currentTarget.value;
    this.setState({isLoading: false, form})
  };


  getUserDetails = async (id) => {
    this.setState({pageLoading: true}, async () => {
      const result = await UsersController.getUserDetails(id)
      if (result) {
        this.setState({form: result, pageLoading: false, photo: result.photo})
      }
    })
  }

  render() {
    const {pageLoading, isLoading, form, errorList} = this.state;
    return (
        <div className="G-admin-pages">
          <div className="G-admin-block">

            {/*  Admin page header path  */}
            <HeaderComponent/>
            {/* Admin page main components */}


            <div className="P-admin-container">
              <div className="P-admin-page-box">
                <div className='P-users-header G-flex G-align-center G-justify-between'>
                  <h3>{this.state.userId ? 'Edit user' : 'New user'}</h3>
                  <span className='P-line'/>
                </div>
                <div className='P-users-body'>
                  <div className='P-user-details'>
                    <div className={`G-input-block ${errorList.name ? 'G-error' : ''}`}>
                      <p>User name</p>
                      <label>
                        <input type="text"
                               onChange={this.changeInput}
                               name={'name'}
                               value={form.name}
                               placeholder='User name'/>
                      </label>
                    </div>
                    <div className='G-upload-image G-flex G-align-center'>
                      <label>
                        <input className="P-input-file" type="file" name="picture"
                               formEncType='multipart/form-data'
                               onChange={this.coverUpload} accept="image/x-png,image/gif,image/jpeg"/>
                        <div className={`P-upload-box ${errorList.photo ? 'G-error' : ''}`}>
                          <span style={{backgroundImage: `url('${uploadIcon}')`}}/>
                          <p>Photo</p>
                        </div>
                      </label>
                      <div className='P-image-result' style={{backgroundImage: `url(${this.state.photo})`}}/>
                    </div>
                    <div className={`G-input-block ${errorList.email || errorList.incorrectEmail ? 'G-error' : ''}`}>
                      <p>Email</p>
                      <label>
                        <input type="text"
                               placeholder='Email'
                               onChange={this.changeInput}
                               name={'email'}
                               value={form.email}
                        />
                      </label>
                    </div>
                    <div className={`G-input-block ${errorList.location ? 'G-error' : ''}`}>
                      <p>Location</p>
                      <label>
                        <input type="text"
                               placeholder='Location'
                               onChange={this.changeInput}
                               name={'location'}
                               value={form.location}
                        />
                      </label>
                    </div>
                    <div className='P-save-btn'>
                      <ButtonLoader buttonText={this.state.userId ? 'Save' : 'Create'} isLoading={isLoading}
                                    onClick={this.submit}/>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {pageLoading ? <PageLoader active={true}/> : null}

        </div>
    );
  }
}

export default SuperAdminUsersDetails;
