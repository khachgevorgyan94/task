import React, {Component} from 'react';
import HeaderComponent from "../../../../../components/admin-page-header";
import './style.scss';
import PageLoader from "../../../../../components/page-loader";
import ROUTES from "../../../../../platform/constants/routes";
import {Link} from "react-router-dom";
import UsersController from "../../../../../platform/api/users";

import deleteIcon from '../../../../../assets/images/delete.png'
import moment from "moment";
// import Pagination from "antd/es/pagination";
import Table from "antd/es/table";

class SuperAdminUsersMain extends Component {
  state = {
    reqData: {
      _sort: [],
      _order: [],
    },
    checkedAllData: {
      isChecked: false,
      checkedData: []
    },
    data: null,
    pageSize: 10,
  }

  componentDidMount() {
    this.getUsersList()
  }

  getUsersList = async () => {
    this.setState({isLoading: true}, async () => {
      const result = await UsersController.getUserList(this.state.reqData)
      if (result.length) {
        this.setState({isLoading: false, data: result})
      }
    })
  }

  changePage = (pageSize) => {
    this.setState({pageSize: pageSize.pageSize})
  }

  disableUser = async (e,item) => {
    e.stopPropagation();
    e.preventDefault();
    const body = {
      disabled: !item.disabled
    }

    this.setState({isLoading: true}, async () => {
      await UsersController.disableUser(item.id, body)
      this.getUsersList()
    })
  }

  deleteUser = async (e,id) => {
    e.stopPropagation();
    e.preventDefault();
    this.setState({isLoading: true}, async () => {
      await UsersController.deleteUser(id)
      this.getUsersList()
    })
  }


  columns = [
    {
      title: 'Photo',
      dataIndex: 'photo',
      render: data => {
        return <div className='P-users-profile G-image-cover'
                    style={{backgroundImage: `url('${data}')`}}/>
      },
      width: 80,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      render:data=><p className='P-ellipsis'>{data}</p>,
    },
    {
      title: 'Location',
      dataIndex: 'location',
      width: 150,
      render:data=><p className='P-ellipsis'>{data}</p>
    },
    {
      title: 'Registered date',
      dataIndex: 'registeredDate',
      render: data => moment(data).format('YYYY MMM DD').valueOf(),
    },
    {
      title: 'Last active date',
      dataIndex: 'lastActiveDate',
      render: data => moment(data).format('YYYY MMM DD').valueOf(),
    },
    {
      title: 'Email',
      dataIndex: 'email',
      render:data=><p className='P-ellipsis'>{data}</p>
    },
    {
      title: 'Action',
      render: data => <div className='P-action-block G-flex G-align-center G-justify-between'>
        <div className={`G-action-switch ${data.disabled ? 'G-checked' : ''}`}
             onClick={(e) => this.disableUser(e,data)}>
          <span/>
        </div>
        <div className='P-delete-user G-image-contain'
             style={{backgroundImage: `url('${deleteIcon}')`}}
             onClick={(e) => this.deleteUser(e,data.id)}
        />
      </div>
    },
  ];

  render() {
    const {isLoading, data, checkedAllData, reqData} = this.state;
    return (
        <div className="G-admin-pages">
          <div className="G-admin-block">

            {/*  Admin page header path  */}
            <HeaderComponent/>
            {/* Admin page main components */}


            <div className="P-admin-container">
              <div className="P-admin-page-box">
                <div className='P-users-header G-flex G-align-center G-justify-between'>
                  <h3>All users</h3>
                  <span className='P-line'/>
                  <div className='P-add-new-user'>
                    <Link to={ROUTES.SUPER_ADMIN.USERS.CREATE}>New user</Link>
                  </div>
                </div>
                <div className='P-users-body'>
                  <div className='P-users-list-block'>
                    {data ? <div className='G-pagination-block'>
                      <Table columns={this.columns}
                             dataSource={data}
                             pagination={{pageSize: this.state.pageSize}}
                             rowKey={record => record.id}
                              onChange={this.changePage}
                             paginatio={{
                               showQuickJumper: false
                             }}
                             onRow={(record, index) => {
                               return {
                                 onClick: e => {
                                   this.props.history.push({pathname: `${ROUTES.SUPER_ADMIN.USERS.DETAILS.replace(':id', record.id)}`})
                                 }
                               }
                             }}
                             scroll={{x: 800}}/>
                    </div> : null}

                  </div>
                </div>
              </div>
            </div>
          </div>
          {isLoading ? <PageLoader active={true}/> : null}
        </div>
    );
  }
}

export default SuperAdminUsersMain;
