const buildContext = (parent, subs) => {
  const routes = {
    valueOf() {
      return parent;
    }, ...subs
  };
  const actions = {
    get(target, key) {
      return parent + target[key];
    }
  };
  return new Proxy(routes, actions);
};

const users = buildContext('/users', {
  MAIN: '',
  DETAILS: '/details/:id',
  CREATE: '/crete'
})

const ROUTES = {
  SUPER_ADMIN: {
    USERS: users
  },

}

export default ROUTES;
