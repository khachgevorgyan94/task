import Settings from "../serivces/settings";

export const UsersStatusTypeEnum = [
  {
    name: Settings.translations.users,
    value: 1,
  },
  {
    name: Settings.translations.driver_text,
    value: 2,
  },
  {
    name: Settings.translations.no_driver_text,
    value: 3,
  }
]

export const UsersOnlineTypeEnum = [

  {
    name: Settings.translations.offline,
    value: 2,
  },
  {
    name: Settings.translations.driver_to_order,
    value: 3,
  },
  {
    name: Settings.translations.driver_busy,
    value: 4,
  },
  {
    name: Settings.translations.driver_is_free,
    value: 5,
  },
  {
    name: Settings.translations.client_in_order,
    value: 6,
  },
  {
    name: Settings.translations.client_busy,
    value: 7,
  },

]


export const GenderTypeEnum = [
  {
    name: Settings.translations.male,
    value: 'male',
  },
  {
    name: Settings.translations.female,
    value: 'female',
  },
]


export const FirstOrderTypeEnum = [
  {
    name: Settings.translations.client_yes,
    value: 1,
  },
  {
    name: Settings.translations.client_no,
    value: 2,
  },
  {
    name: Settings.translations.driver_yes,
    value: 3,
  },
  {
    name: Settings.translations.driver_no,
    value: 4,
  },
  {
    name: Settings.translations.all_yes,
    value: 5,
  },
  {
    name: Settings.translations.all_no,
    value: 6,
  },
]

export const DeviceVersionTypeEnum = [
  {
    name: '1.1.0',
    value: 1,
  },
  {
    name: '1.1.2',
    value: 2,
  },
  {
    name: '1.1.3',
    value: 3,
  },
  {
    name: '1.1.4',
    value: 4,
  },
]

export const UserTypeEnum = [
  {
    name: Settings.translations.users,
    value: 1,
  },
  {
    name: Settings.translations.super_users,
    value: 2,
  },
]

export const DeviceType = [
  {
    name: Settings.translations.android,
    value: 1,
  },
  {
    name: Settings.translations.ios,
    value: 2,
  },
  {
    name: Settings.translations.web,
    value: 3,
  },
]

export const PhoneCountryList = [
  {
    name: 'Armenia',
    value: 1,
  },
  {
    name: 'Russia',
    value: 2,
  },
  {
    name: 'USA',
    value: 3,
  },

]

export const BlackList = [
  {
    name: Settings.translations.black_list_client_yes,
    value: 1,
  },
  {
    name: Settings.translations.black_list_client_no,
    value: 2,
  },
  {
    name: Settings.translations.black_list_driver_yes,
    value: 3,
  },
  {
    name: Settings.translations.black_list_driver_no,
    value: 4,
  },
  {
    name: Settings.translations.all_yes,
    value: 5,
  },
  {
    name: Settings.translations.all_no,
    value: 6,
  },
]


export const PhotoControlTypeEnum = [
  {
    name: Settings.translations.passed,
    value: 1,
  },
  {
    name: Settings.translations.pending_text,
    value: 2,
  },
  {
    name: Settings.translations.rejected_text,
    value: 3,
  },
  {
    name: Settings.translations.overdue,
    value: 4,
  },
  {
    name: Settings.translations.not_promoted,
    value: 5,
  },
  {
    name: Settings.translations.order_in_pending_text,
    value: 6,
  },
]


export const CountryPassportList = [
  {
    name: 'Armenia -1',
    value: 1,
  },
  {
    name: 'Armenia -2',
    value: 2,
  },
  {
    name: 'Armenia -3',
    value: 3,
  },
  {
    name: 'Armenia -4',
    value: 4,
  },
]

export const VerificationStatusType = [
  {
    name: Settings.translations.accepted_text,
    value: 1,
  },
  {
    name: Settings.translations.pending_text,
    value: 2,
  },
  {
    name: Settings.translations.rejected_text,
    value: 3,
  },
  {
    name: Settings.translations.not_promoted,
    value: 4,
  },

]


export const VerificationStatus = [
  {
    name: Settings.translations.rejected_text,
    value: 2
  },
  {
    name: Settings.translations.pending_text,
    value: 1
  }
]

export const CategoryTypeEnum = [
  {
    name: Settings.translations.systemic,
    value: 1
  },
  {
    name: Settings.translations.message,
    value: 2
  }
]

export const UsersOrderStatus = [
  {
    name: Settings.translations.client_text,
    value: 1,
  },
  {
    name: Settings.translations.driver_text,
    value: 2,
  },

]

export const ChangeDriversStatus = [
  {
    name: Settings.translations.free_text,
    value: 1,
  },
  {
    name: Settings.translations.busy_text,
    value: 2,
  },  {
    name: Settings.translations.in_order,
    value: 3,
  },

]
export const ChangeClientStatus = [
  {
    name: Settings.translations.free_text,
    value: 1,
  },
  {
    name: Settings.translations.busy_text,
    value: 2,
  },

]



