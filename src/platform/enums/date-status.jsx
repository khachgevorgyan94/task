import Settings from "../serivces/settings";

export const DateStatusList = [
  {
    name: Settings.translations.to_day,
    value: 1
  },
  {
    name: Settings.translations.yesterday,
    value: 2
  },
  {
    name: Settings.translations.last_week,
    value: 3
  },
  {
    name: Settings.translations.this_month,
    value: 4
  }, {
    name: Settings.translations.last_month,
    value: 5
  },
  {
    name: Settings.translations.this_year,
    value: 6
  },
  {
    name: Settings.translations.last_year,
    value: 7
  }
]


export const GetDateStatus = (options) => {
  const toDay = new Date();
  const toDayNew = new Date();
  const filter = {
    dateFrom: null,
    dateTo: null,
  }
  switch (options.value) {
    case 1: {
      filter.dateFrom = toDay;
      filter.dateTo = toDay
      break;

    }
    case 2: {
      filter.dateFrom = new Date(toDay.setDate(toDay.getDate() - 1));  // yesterday
      filter.dateTo = toDayNew;
      break;
    }
    case 3: {
      filter.dateFrom = new Date(toDay.getFullYear(), toDay.getMonth(), toDay.getDate() - 7); //  last week
      filter.dateTo = toDayNew;
      break;
    }
    case 4: {
      let date = new Date(), y = date.getFullYear(), m = date.getMonth();
      filter.dateFrom = new Date(y, m, 1); //  this month week
      filter.dateTo = toDayNew;
      break;
    }
    case 5: {
      filter.dateFrom = new Date(toDay.setMonth(toDay.getMonth() - 1));  //last month
      filter.dateTo = toDayNew;
      break;
    }
    case 6: {
      let date = new Date(), y = date.getFullYear();
      filter.dateFrom = new Date(y, 0, 1);  //this year
      filter.dateTo = toDayNew;
      break;
    }
    case 7: {
      let date = new Date(), y = date.getFullYear() - 1;
      filter.dateFrom = date.setFullYear(y);  //this year
      filter.dateTo = toDayNew;
      break;
    }
    default: {
      filter.dateFrom = null;  //this year
      filter.dateTo = null;
    }
  }
  let timeTo = new Date(toDayNew)
  let timeFrom = new Date(filter.dateFrom)

  timeTo.setHours(23, 59)
  timeFrom.setHours(0, 0)
  filter.dateTo = options.value ? timeTo : null
  filter.dateFrom = options.value ? timeFrom : null
  return filter
}
