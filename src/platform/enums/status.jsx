import Settings from "../serivces/settings";

export const StatusEnum = [
  {
    name: Settings.translations.pending_text,
    value: 1
  },
  {
    name: Settings.translations.rejected_text,
    value: 2
  }

]


export const ChangeStatusEnumUsers = {
  free: 1,
  busy: 2,
  inOrder: 3

}


export const getStatusEnum = (value) => {
  let text
  StatusEnum.map(item => {

    if (item.value === value) {
      text = item.name
    }
  })
  return text
}


