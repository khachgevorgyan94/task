import Connection from '../serivces/connection';

const controllerName = 'users';

class UsersController {

  static getUserList = (body) => {
    const result = Connection.GET(controllerName, '', body);
    return result;
  };

  static disableUser = (id, body) => {
    const result = Connection.PATCH(controllerName, `${id}`, body, '', false);
    return result;
  };

  static deleteUser = (id) => {
    const result = Connection.DELETE(controllerName, `${id}`,);
    return result;
  };

  static editUser = (body) => {
    const result = Connection.PUT(controllerName, `${body.id}`, body, '', false);
    return result;
  };

  static createUser = (body) => {
    const result = Connection.POST(controllerName, ``, body, '', false);
    return result;
  };

  static getUserDetails = (id) => {
    const result = Connection.GET(controllerName, `${id}`);
    return result;
  };


  static uploadImage = (id,body) => {
    const result = Connection.POST(controllerName, ``, body, '', true);
    return result;
  };
}

export default UsersController;

