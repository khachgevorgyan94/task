class UserValidation {

  static isValidEmail = (value) => {
    if (!value && value !== '') return false;
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    return regex.test(value);
  };

  static isValidation = (form) => {
    let isValidation = true

    const errorList = {
      name: false,
      email: false,
      location: false,
      incorrectEmail: false,
    }

    if (!form.name.length) {
      errorList.name = true
      isValidation = false
    }

    if (!form.location.length) {
      errorList.location = true
      isValidation = false
    }
    if (!form.email.length) {
      errorList.email = true
      isValidation = false
    }
    if (form.email.length && !this.isValidEmail(form.email)) {
      errorList.incorrectEmail = true
      isValidation = false
    }

    return {
      isValidation: isValidation,
      errorList: errorList
    }
  }
}


export default UserValidation
