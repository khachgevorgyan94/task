import {
  routerListSuperAdmin,
    SuperAdminSideBar
} from "../../modules/super-admin/router-services";

class RouteServices {

  static getRoleRouter = () => {
    let role = localStorage.getItem('role');
    let token = localStorage.getItem('token');
    token = 'TokenTest'  // custom data
    role = 'SuperAdmin'  // custom data

    switch (role) {
      case 'SuperAdmin': {
        if (token) {
          return routerListSuperAdmin
        }
        break;
      }
      default : {
        return routerListSuperAdmin
      }
    }
  }
  static  getSideBarList = () => {
    const role = localStorage.getItem('role');
    switch (role) {
      case 'SuperAdmin': {
        return SuperAdminSideBar
      }
      default : {
        return SuperAdminSideBar
      }
    }
  }

  static isRole = () => {
    let role = localStorage.getItem('role');
    role = 'SuperAdmin'  // custom data

    switch (role) {
      case 'SuperAdmin': {
        return true
      }
      default : {
        return false
      }
    }
  }


}

export default RouteServices
